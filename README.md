#需要初始化的设置
``
ItemIcon.SetIcon = (rawImage, id) => ModUtil.SetConfigIcon(rawImage, id);
InventoryUtil.GetMax = ItemUtil.GetMax;
InventoryUtil.OnStartPick += () => { InputDispatcher.AddListener(InputActionName.Escape, InputActionPhase.Performed, InventoryUtil.CancelPickingItem); };
InventoryUtil.OnCancelPick += () => { InputDispatcher.RemoveListener(InputActionName.Escape, InputActionPhase.Performed, InventoryUtil.CancelPickingItem); };
``
