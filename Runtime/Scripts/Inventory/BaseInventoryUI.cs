﻿using UnityEngine;

namespace Jinndev.GameKit {

    /// <summary>
    /// 基础库存UI
    /// </summary>
    public class BaseInventoryUI : BaseUIBehaviour {

        /// <summary>
        /// 存有物品数据的库存
        /// </summary>
        public Inventory Inventory { get; private set; }

        public void Init(Inventory inventory) {
            Inventory = inventory;
        }

    }

}
