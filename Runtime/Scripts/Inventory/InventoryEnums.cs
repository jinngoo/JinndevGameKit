using UnityEngine;

namespace Jinndev.GameKit {

    /// <summary>
    /// 视图类型
    /// </summary>
    public enum ViewType : int {
        /// <summary>
        /// 使用GridView
        /// </summary>
        [Tooltip("使用GridView")]
        GridView,
        /// <summary>
        /// 直接将RectTransform用作容器
        /// </summary>
        [Tooltip("直接将RectTransform用作容器")]
        Container,
    }

    /// <summary>
    /// 拾取操作类型
    /// </summary>
    public enum PickAction {
        /// <summary>
        /// 拾取全部
        /// </summary>
        [Tooltip("拾取全部")]
        PickAll,
        /// <summary>
        /// 拾取一半
        /// </summary>
        [Tooltip("拾取一半")]
        PickHalf,
        /// <summary>
        /// 禁用
        /// </summary>
        [Tooltip("禁用")]
        Disable,
    }

    /// <summary>
    /// 放下操作类型
    /// </summary>
    public enum DropAction {
        /// <summary>
        /// 放下全部
        /// </summary>
        [Tooltip("放下全部")]
        DropAll,
        /// <summary>
        /// 放下一个
        /// </summary>
        [Tooltip("放下一个")]
        DropOne,
        /// <summary>
        /// 禁用
        /// </summary>
        [Tooltip("禁用")]
        Disable,
        /// <summary>
        /// 取消拾取
        /// </summary>
        [Tooltip("取消拾取")]
        CancelPick,
    }

    /// <summary>
    /// 拿起物品时的行为
    /// </summary>
    public enum PickBehaviour {
        /// <summary>
        /// 直接拿起
        /// </summary>
        [Tooltip("直接拿起")]
        Pick,
        /// <summary>
        /// 复制一份拿起，并且不能放回
        /// </summary>
        [Tooltip("复制一份拿起，并且不能放回")]
        Copy,
        /// <summary>
        /// 禁用
        /// </summary>
        [Tooltip("禁用")]
        Disable
    }

    /// <summary>
    /// 放入自身库存时的行为
    /// </summary>
    public enum DropIntoSelfInventory {
        /// <summary>
        /// 放下/合并/交换
        /// </summary>
        [Tooltip("放下/合并/交换")]
        DropCombineSwap,
        /// <summary>
        /// 禁用
        /// </summary>
        [Tooltip("禁用")]
        Disable,
    }

    /// <summary>
    /// 放入其他库存时的行为
    /// </summary>
    public enum DropIntoOtherInventory {
        /// <summary>
        /// 放回原处
        /// </summary>
        [Tooltip("放回原处")]
        Cancel,
        /// <summary>
        /// 销毁
        /// </summary>
        [Tooltip("销毁")]
        Destroy,
        /// <summary>
        /// 由接收者决定
        /// </summary>
        [Tooltip("由接收者决定")]
        ByReceiver,
    }

    /// <summary>
    /// 接收其他库存物品时的行为
    /// </summary>
    public enum ReceiveFromOtherInventory {
        /// <summary>
        /// 禁用
        /// </summary>
        [Tooltip("禁用")]
        Disable,
        /// <summary>
        /// 放下/合并/交换
        /// </summary>
        [Tooltip("放下/合并/交换")]
        DropCombineSwap,
        /// <summary>
        /// 覆盖，强制放入
        /// </summary>
        [Tooltip("覆盖，强制放入")]
        Override,
        /// <summary>
        ///  复制一份覆盖放入，并尝试放回原物
        /// </summary>
        [Tooltip(" 复制一份覆盖放入，并尝试放回原物")]
        CopyOverride,
        /// <summary>
        /// 放回原物并拿起当前格子
        /// </summary>
        [Tooltip("放回原物并拿起当前格子")]
        CancelPick,
    }

    /// <summary>
    /// 放入背景界面时的行为
    /// </summary>
    public enum DropOnBackground {
        /// <summary>
        /// 放回原处
        /// </summary>
        [Tooltip("放回原处")]
        PutBack,
        /// <summary>
        /// 扔掉
        /// </summary>
        [Tooltip("扔掉")]
        Drop,
        /// <summary>
        /// 销毁
        /// </summary>
        [Tooltip("销毁")]
        Destroy,
    }

}