﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Jinndev.GameKit {

    /// <summary>
    /// 物品库存数据，支持作为ECS的DynamicBuffer元素
    /// </summary>
    public struct Item : IBufferElementData, IEquatable<Item> {

        /// <summary>
        /// 空的物品ID，可在初始化时自定义，默认0
        /// </summary>
        public const int EmptyID = 0;

        public static readonly Item Empty = new Item { id = EmptyID };

        public int id;
        /// <summary>
        /// 当前数量，当max>=0时有效
        /// </summary>
        public int num;
        /// <summary>
        /// 最大堆叠, =0无限堆叠，=-1则无数量概念
        /// </summary>
        public int max;

        public Item(int id, int num, int max) {
            this.id = id;
            this.num = num;
            this.max = max;
        }

        /// <summary>
        /// 是否非空
        /// </summary>
        /// <returns></returns>
        public bool HasItem() {
            return id != EmptyID && num > 0;
        }

        /// <summary>
        /// 是否是空ID
        /// </summary>
        public bool IsEmptyID() {
            return id == EmptyID;
        }

        /// <summary>
        /// 尝试增加数量(正/负)
        /// </summary>
        /// <returns>剩下的数量</returns>
        public int Add(int delta) {
            if (delta > 0) {
                int min = max > 0 ? math.min(delta, max - num) : delta;
                if (min > 0) {
                    num += min;
                    return delta - min;
                }
            }
            else if (delta < 0) {
                int max = math.max(delta, 0 - num);
                if (max < 0) {
                    num += max;
                    return delta - max;
                }
            }
            return 0;
        }


        /// <summary>
        /// 无视最大堆叠而直接设置数量
        /// </summary>
        /// <param name="num"></param>
        /// <param name="setEmptyOnZero">数量为0时是否设置为EmptyID</param>
        public void SetNum(int num, bool setEmptyOnZero) {
            if (num > 0 && id == EmptyID) {
                Debug.LogError("Attempt to set num to an empty item");
                return;
            }
            this.num = num;
            if (num == 0 && setEmptyOnZero) {
                id = EmptyID;
                max = 0;
            }
        }

        //public Item Split(int num, bool setEmptyOnZero) {
        //    if(num <= 0 || this.num <= 0) {
        //        return Empty;
        //    }
        //    num = Mathf.Min(num, this.num);
        //    Item newItem = new Item(id, num, max);
        //    SetNum(this.num - num, setEmptyOnZero);
        //    return newItem;
        //}

        public override string ToString() {
            return $"({id}:{num}/{max})";
        }

        public bool Equals(Item other) {
            return other.id == id && other.num == num;
        }

        #region static
        /// <summary>
        /// 仅有id，用于图标显示
        /// </summary>
        public static Item CreateForIcon(int id) {
            return new Item {
                id = id,
                num = 1,
                max = -1
            };
        }
        #endregion


    }

}
