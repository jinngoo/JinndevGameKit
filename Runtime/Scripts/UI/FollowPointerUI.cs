﻿using Jinndev.UI;
using UnityEngine;

namespace Jinndev.GameKit {

    public class FollowPointerUI : MonoBehaviour {

        public Vector2 offset = new Vector2(0, 0);

        private RectTransform rectTransform;

        private void Awake() {
            rectTransform = GetComponent<RectTransform>();
        }

        public void Init(Vector2 size, Vector2 offset) {
            this.offset = offset;
            Init(size);
        }

        public void Init(Vector2 size) {
            rectTransform.sizeDelta = size;

            Update();
        }

        private void Update() {
            Vector2 screenPos = ViewManager.Instance.PointerScreenPosition;
            rectTransform.anchoredPosition = UIManager.Instance.ScreenToAnchoredPosition(screenPos) + offset;
        }

    }

}
