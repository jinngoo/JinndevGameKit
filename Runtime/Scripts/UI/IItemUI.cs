﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Jinndev.GameKit {
    
    /// <summary>
    /// 展示Item的相关UI
    /// </summary>
    public interface IItemUI {

        GameObject gameObject { get; }

        Transform transform { get; }

        ItemIconSetting Setting { get; }

        int Index { get; set; }

        Item Item { get; set; }

        void Init(ItemIconSetting setting, PointerHandler pointerHandler = null, System.Action<IItemUI, PointerEventData> onPointerClick = null);

        void Set(Item item, int index, bool selected = false);

        void SetSelected(bool selected);

        void EnableFloatingStyle();

    }

}
