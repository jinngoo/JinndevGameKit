using UnityEngine;

namespace Jinndev.GameKit {

    /// <summary>
    /// 图标设置
    /// </summary>
    public struct ItemIconSetting {

        /// <summary>
        /// 是否背景
        /// </summary>
        public bool showBg;

        /// <summary>
        /// 是否显示序号
        /// </summary>
        public bool showIndex;

        /// <summary>
        /// 起始序号
        /// </summary>
        public int indexOffset;

        /// <summary>
        /// 是否显示数量
        /// </summary>
        public bool showNum;

        /// <summary>
        /// 是否显示最大数量
        /// </summary>
        public bool showMaxNum;

        /// <summary>
        /// 是否可以选中
        /// </summary>
        public bool showSelected;

        /// <summary>
        /// 数量的显示格式(可选)
        /// </summary>
        public string numFormat;

    }

}