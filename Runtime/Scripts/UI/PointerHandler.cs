using UnityEngine;

namespace Jinndev.GameKit {

    /// <summary>
    /// 指针事件回调处理器
    /// </summary>
    public interface PointerHandler {

        /// <summary>
        /// 指针进入
        /// </summary>
        void Enter(int index, Item item, RectTransform transform);

        /// <summary>
        /// 指针离开
        /// </summary>
        void Exit(int index, Item item);

    }

}